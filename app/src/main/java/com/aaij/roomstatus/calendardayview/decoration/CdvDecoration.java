package com.aaij.roomstatus.calendardayview.decoration;

import android.graphics.Rect;

import com.aaij.roomstatus.calendardayview.DayView;
import com.aaij.roomstatus.calendardayview.EventView;
import com.aaij.roomstatus.calendardayview.PopupView;
import com.aaij.roomstatus.calendardayview.data.IEvent;
import com.aaij.roomstatus.calendardayview.data.IPopup;

/**
 * Created by FRAMGIA\pham.van.khac on 22/07/2016.
 */
public interface CdvDecoration {

    EventView getEventView(IEvent event, Rect eventBound, int hourHeight, int seperateHeight);

    PopupView getPopupView(IPopup popup, Rect eventBound, int hourHeight, int seperateHeight);

    DayView getDayView(int hour);
}
