package com.aaij.roomstatus;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiService {
    @GET("getRuang.php")
    Call<JSONResponse> getRuangan();

    @GET("getDetailRoom2.php")
    Call<JSONResponse> getDetailRuang(@Query("idruang") String idruang,
                                      @Query("tgl") String tgl);

    @GET("searchRuang.php")
    Call<JSONResponse> getAll(@Query("idruang") String idruang,
                              @Query("tgl") String tgl);

    @GET("getUser.php")
    Call<JSONResponse> getUser(@Query("npk") String npk);

    @GET("getKaryawan.php")
    Call<JSONResponse> getKaryawan(@Query("npk") String npk);

    //timevalidation
    @GET("TimeValidation.php")
    Call<JSONResponse> getTimeValid(@Query("idr") String idr,
                                    @Query("tgl") String tgl,
                                    @Query("jam1") String jam1,
                                    @Query("jam2") String jam2);

    @FormUrlEncoded
    @POST("insertBooking2.php")
    Call<Result> insertBooking(@Field("npk") String npk,
                               @Field("nama") String nama,
                               @Field("id_ruang") String id_ruang,
                               @Field("tgl_msk") String tgl_msk,
                               @Field("tgl_sl") String tgl_sl,
                               @Field("jam_msk") String jam_msk,
                               @Field("jam_sl") String jam_sl,
                               @Field("ket") String ket,
                               @Field("appr") String appr);

}
