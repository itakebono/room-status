package com.aaij.roomstatus;

import com.google.gson.annotations.SerializedName;

public class Result {
    @SerializedName("error")
    private Boolean error;

    @SerializedName("message")
    private String message;

    @SerializedName("value")
    private String value;

    public Result(Boolean error, String value, String message) {
        this.value = value;
        this.error = error;
        this.message = message;
    }

    public Boolean getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    public String getValue(){
        return value;
    }

}
