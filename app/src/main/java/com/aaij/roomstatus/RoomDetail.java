package com.aaij.roomstatus;

import android.app.ActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RoomDetail extends AppCompatActivity {
    TextView tJudul, tTgl, tKet, tPIC, tInfo,tDate, tTime;
    ArrayList<Pesan> pesans;
    ArrayList<User> users;
    private Calendar mCalendar;
    private int mYear, mMonth, mHour, mMinute, mDay, mSecond;
    private String mDate, ntgl, jamMulai, jamSelesai;
    ProgressBar pb;
    RelativeLayout bg;
    LinearLayout btnBook, Lwaktu, Luser;
    ImageView pic,arrow, iNext,iPerson;
    private static final int led_blue = 3;
    private static final int led_green = 4;
    private static final int led_off = 0;
    private static final int led_on = 1;
    private static final int led_red = 2;
    private static final int seek_blue = 163;
    private static final int seek_green = 162;
    private static final int seek_red = 161;
    private final Handler handler = new Handler();
    SimpleDateFormat format;
    String ruang, idruang, img,s,t,appr;
    int i=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.isi_detail_book);
        //jnielc.open();
        //Sampler.getInstance().stop();
        //this.mhandler.post(this.mTask);
        //set_led_color(1);
        this.getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        tJudul = (TextView)findViewById(R.id.nmRuang);
        tTgl = (TextView)findViewById(R.id.Tgl);
        tKet = (TextView)findViewById(R.id.kep);
        /*tKet.setSelected(true);
        tKet.setSingleLine(true);
        tKet.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        tKet.setHorizontallyScrolling(true);
        tKet.setMarqueeRepeatLimit(-1);
        tKet.setFocusable(true);
        tKet.setFocusableInTouchMode(true);*/

        tPIC    = (TextView)findViewById(R.id.member);
        tInfo   = (TextView)findViewById(R.id.wkt);
        tDate   = (TextView)findViewById(R.id.date);
        tTime   = (TextView)findViewById(R.id.time);
        pic     = (ImageView)findViewById(R.id.img);
        bg      = (RelativeLayout)findViewById(R.id.card);
        arrow   =(ImageView)findViewById(R.id.bck);
        iNext   =(ImageView)findViewById(R.id.next);
        btnBook = (LinearLayout)findViewById(R.id.booknow);
        Lwaktu  = (LinearLayout)findViewById(R.id.waktu);
        //iPerson = (ImageView)findViewById(R.id.person);
        ruang   = getIntent().getStringExtra("ruang");
        idruang = getIntent().getStringExtra("idruang");
        img     = getIntent().getStringExtra("img");
        appr    = getIntent().getStringExtra("appr");

        pb = (ProgressBar)findViewById(R.id.progressBar);
        pb.setVisibility(View.INVISIBLE);
        tJudul.setText(ruang);
        Locale id = new Locale("id","ID");
        s  = (String) DateFormat.format("d MMMM yyyy", Calendar.getInstance(id));
        t  = (String) DateFormat.format("dd-MM-yyyy", Calendar.getInstance(id));
        tDate.setText(t);
        ntgl = t;

        arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        iNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RoomDetail.this, NextEvent.class);
                i.putExtra("tgl", ntgl);
                i.putExtra("idruang", idruang);
                //i.putExtra("idruang","18");
                //i.putExtra("idruang","8");
                startActivity(i);
            }
        });

        setTheme(android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        loadDatas(ntgl);
        tTime.setText(new SimpleDateFormat("HH:mm", Locale.US).format(new Date()));
//        tDate.setText(format.format(mCalendar.getTime()));
        /*final Handler someHandler = new Handler(getMainLooper());
        final Handler handler = new Handler(getMainLooper());
        *//*someHandler.postDelayed(new Runnable() {
            @Override
            public void run() {

                someHandler.postDelayed(this, 1000);
            }
        }, 10);*//*

        handler.postDelayed(new Runnable() {
            public void run() {
                loadDatas();
                tTime.setText(new SimpleDateFormat("HH:mm", Locale.US).format(new Date()));
                tDate.setText(format.format(mCalendar.getTime()));
                handler.postDelayed(this, 10000);
            }
        }, 10000);*/
        RefreshHalaman();
        //RefreshTime();
        btnBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RoomDetail.this, BookNow.class);
                i.putExtra("tgl", ntgl);
                i.putExtra("idruang", idruang);
                i.putExtra("appr", appr);
                //i.putExtra("idruang","8");
                i.putExtra("ruang", ruang);
                //i.putExtra("ruang", "Ambon Room");
                startActivityForResult(i,1);
            }
        });

    }

    private void RefreshHalaman() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                loadDatas(ntgl);
                i++;
                Log.d("refresh",String.valueOf(i));
                Locale id = new Locale("id","ID");
                s  = (String) DateFormat.format("d MMMM yyyy", Calendar.getInstance(id));
                t  = (String) DateFormat.format("dd-MM-yyyy", Calendar.getInstance(id));
                tDate.setText(t);
                ntgl = t;
                tTime.setText(new SimpleDateFormat("HH:mm", Locale.US).format(new Date()));
                RefreshHalaman();
            }
        }, 10000);
    }

    private void RefreshTime() {

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                tTime.setText(new SimpleDateFormat("HH:mm", Locale.US).format(new Date()));
                RefreshTime();
            }
        }, 10000);
    }

    private void loadDatas(String tgl) {
        koneksi();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Utils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService request = retrofit.create(ApiService.class);
        Call<JSONResponse> call = request.getDetailRuang(idruang.trim(), tgl.trim());
        call.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {
                JSONResponse jsonResponse = response.body();
                pesans = new ArrayList<>(Arrays.asList(jsonResponse.getPesans()));
                //Toast.makeText(RoomDetail.this, id+ntgl, Toast.LENGTH_SHORT).show();
                if (pesans.size() > 0) {

                    btnBook.setVisibility(View.GONE);
                    tTgl.setText(pesans.get(0).getTgl_msk());
                    tJudul.setText(ruang);
                    tDate.setText(pesans.get(0).getTgl_msk());
                    tKet.setText(pesans.get(0).getKet());
                    tInfo.setText(pesans.get(0).getJam_msk()+" - "+pesans.get(0).getJam_kl());
                    Picasso.get().load(Utils.BASE_URL+img).placeholder(R.drawable.room).into(pic);
                    tPIC.setText("Booked by "+pesans.get(0).getNama());
                    Lwaktu.setVisibility(View.VISIBLE);
                }else{
                    btnBook.setVisibility(View.VISIBLE);
                    tTgl.setText("PIC");
                    tJudul.setText(ruang);
                    tPIC.setText("NOW AVAILABLE");
                    tKet.setText("No event at this time");
                    tInfo.setText("Meeting Time");
                    Lwaktu.setVisibility(View.GONE);
                    Picasso.get().load(Utils.BASE_URL+img).placeholder(R.drawable.room).into(pic);
                }
            }
            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                //pb.setVisibility(View.INVISIBLE);
                Log.d("Error", t.getMessage());
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG);
            }
        });
    }

    private boolean adaInternet(){
        ConnectivityManager koneksi = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return koneksi.getActiveNetworkInfo() != null;
    }
    private void koneksi(){
        if(adaInternet()){
            //Toast.makeText(HalamanUtama.this, "Terhubung ke internet", Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(getApplicationContext(), "Tidak ada koneksi internet", Toast.LENGTH_LONG).show();

        }
    }
    @Override
    public void onResume(){
        super.onResume();
        this.getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        loadDatas(ntgl);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:
                //onBackPressed();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        //loadDatas(ntgl);
        onBackPressed();
        /*final AlertDialog.Builder dNote = new AlertDialog.Builder(RoomDetail.this);
        dNote.setTitle("Confirmation\n\n");
        LinearLayout layout = new LinearLayout(RoomDetail.this);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setPadding(25,25,25,25);
        layout.setBackgroundColor(Color.parseColor("#eeeeee"));
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        lp.setMargins(0,0,0,30);
        final TextView theader1 = new TextView(RoomDetail.this);
        theader1.setText("Password");
        theader1.setTextColor(Color.BLACK);
        theader1.setTextSize(20);
        final EditText Epass = new EditText(RoomDetail.this);
        Epass.setHint("Password");
        Epass.setTransformationMethod(PasswordTransformationMethod.getInstance());
        Epass.setTextColor(Color.BLACK);
        Epass.setTextSize(20);

        layout.addView(theader1);
        layout.addView(Epass);

        dNote.setView(layout);
        dNote.setNeutralButton("CANCEL",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int which) {
                        dialog.dismiss();
                    }});
        dNote.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int which) {
                        if(Epass.getText().toString().equals("kosongin")) {
                            dialog.dismiss();
                            startActivity(new Intent(RoomDetail.this, MainActivity.class));
                        }else{
                            dialog.dismiss();
                        }
                    }});
        dNote.show();*/
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 0) {
            return;
        }
        if (requestCode == 1 ) {
            if (data != null) {
                this.getWindow().getDecorView().setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                loadDatas(ntgl);
            }
        }
    }

    public void set_led_color(int freq) {
        //Toast.makeText(getApplicationContext(), String.valueOf(freq), Toast.LENGTH_SHORT).show();
        SharedPreferences.Editor save_editor = getSharedPreferences("addata", 0).edit();
        save_editor.putString("ledcolor", String.valueOf(freq));
        save_editor.commit();
    }

    private void set_led_brightness(int freq) {
        SharedPreferences.Editor save_editor = getSharedPreferences("addata", 0).edit();
        save_editor.putString("ledbrightness ", String.valueOf(freq));
        save_editor.commit();
    }

    @Override
    protected void onPause() {
        super.onPause();

        ActivityManager activityManager = (ActivityManager) getApplicationContext()
                .getSystemService(Context.ACTIVITY_SERVICE);

        //activityManager.moveTaskToFront(getTaskId(), 0);
    }
}
