package com.aaij.roomstatus;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RuangAdapter extends RecyclerView.Adapter<RuangAdapter.ViewHolder> {
    private ArrayList<Ruang> ruangs;
    Context context;
    private RuangAdapter.OnViewClick onViewClick;

    public RuangAdapter(Context context, ArrayList<Ruang> ruangs) {
        this.context = context;
        this.ruangs = ruangs;
    }

    @Override
    public RuangAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.isi_ruang, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(RuangAdapter.ViewHolder holder, int position) {
        holder.tnm.setText(ruangs.get(position).getRoom_name());
        holder.tsts.setText(ruangs.get(position).getID());

        if(ruangs.get(position).getImg_url() != null) {
            Picasso.get().load(Utils.BASE_URL+ruangs.get(position).getImg_url()).placeholder(R.drawable.room).into(holder.img);
        }else {
            Picasso.get().load(R.drawable.room).placeholder(R.drawable.room).into(holder.img);
        }
    }
    public void setOnViewClick(final OnViewClick onViewClick) {
        this.onViewClick = onViewClick;
    }

    public interface OnViewClick {
        void OnViewClick(View view, int position, Ruang model);
    }

    @Override
    public int getItemCount() {
        return ruangs.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tno, tnm, talmt, tsts;
        CardView cardView;
        ImageView img;

        public ViewHolder(final View itemView) {
            super(itemView);
            tnm = (TextView) itemView.findViewById(R.id.nmRuang);
            tsts= (TextView) itemView.findViewById(R.id.status);
            cardView = (CardView)itemView.findViewById(R.id.card);
            img = (ImageView)itemView.findViewById(R.id.img);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // get position
                    int pos = getAdapterPosition();
                    Ruang clickedDataItem = ruangs.get(pos);
                    Intent intent = new Intent(context, RoomDetail.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("idruang", ruangs.get(pos).getID());
                        intent.putExtra("ruang", ruangs.get(pos).getRoom_name());
                        intent.putExtra("img", ruangs.get(pos).getImg_url());
                        intent.putExtra("appr", ruangs.get(pos).getAppr());
                        context.startActivity(intent);
                        //Toast.makeText(v.getContext(), "You clicked " + clickedDataItem.getID(), Toast.LENGTH_SHORT).show();
                    }
            });
        }

        @Override
        public void onClick(View view) {
        }
    }
}