package com.aaij.roomstatus;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.aaij.roomstatus.calendardayview.CalendarDayView;
import com.aaij.roomstatus.calendardayview.EventView;
import com.aaij.roomstatus.calendardayview.PopupView;
import com.aaij.roomstatus.calendardayview.data.IEvent;
import com.aaij.roomstatus.calendardayview.data.IPopup;
import com.aaij.roomstatus.calendardayview.decoration.CdvDecorationDefault;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NextEvent extends AppCompatActivity {

    TextView tjudul, tinfo, truang, ttgl;
    RecyclerView recyclerView;
    DetailAdapter pesanAdapter;
    ArrayList<Pesan> pesans;
    SwipeRefreshLayout swipeRefreshLayout;
    private List<Pesan> pesanList = new ArrayList<>();
    private List<Waktu> waktuList = new ArrayList<>();
    final ArrayList<Pesan> objects = new ArrayList<Pesan>();
    CalendarDayView dayView;
    ArrayList<IEvent> events;
    ArrayList<IPopup> popups;
    final ArrayList<Pesan> objek = new ArrayList<Pesan>();
    ArrayList<User> users;
    int eventColor;
    Toolbar toolbar;
    ActionBar actionBar;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Nullable
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        this.getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        actionBar.setDisplayHomeAsUpEnabled(true);
        //this.setFinishOnTouchOutside(true);
        tinfo = (TextView) findViewById(R.id.textkoneksi);
        tinfo.setVisibility(View.GONE);
        truang = (TextView) findViewById(R.id.ruang);
        ttgl = (TextView) findViewById(R.id.tanggal);
        dayView = (CalendarDayView) findViewById(R.id.calendar);
        dayView.setLimitTime(7, 17);


        ((CdvDecorationDefault) (dayView.getDecoration())).setOnEventClickListener(
                new EventView.OnEventClickListener() {
                    @Override
                    public void onEventClick(EventView view, IEvent data) {
                        Log.e("TAG", "onEventClick:" + data.getName());
                        //view.getHeaderHeight();
                    }

                    @Override
                    public void onEventViewClick(View view, EventView eventView, IEvent data) {
                        Log.e("TAG", "onEventViewClick:" + data.getName());
                        if (data instanceof Event) {
                            // change event (ex: set event color)
                            dayView.setEvents(events);
                        }
                    }
                });

        ((CdvDecorationDefault) (dayView.getDecoration())).setOnPopupClickListener(
                new PopupView.OnEventPopupClickListener() {
                    @Override
                    public void onPopupClick(PopupView view, IPopup data) {
                        Log.e("TAG", "onPopupClick:" + data.getTitle());
                    }

                    @Override
                    public void onQuoteClick(PopupView view, IPopup data) {
                        Log.e("TAG", "onQuoteClick:" + data.getTitle());
                    }

                    @Override
                    public void onLoadData(PopupView view, ImageView start, ImageView end,
                                           IPopup data) {
                        start.setImageResource(R.drawable.ic_person);
                    }
                });

        Intent i = getIntent();
        i.getStringExtra("idruang");
        truang.setText(i.getStringExtra("idruang"));
        ttgl.setText(i.getStringExtra("tgl"));
        Toast.makeText(NextEvent.this, i.getStringExtra("idruang") + ttgl.getText().toString(), Toast.LENGTH_SHORT);

        Button btnbook = (Button) findViewById(R.id.booknow);
        loadDatas();
    }

    private void loadDatas() {
        koneksi();
        String id = getIntent().getStringExtra("idruang").trim();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Utils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService request = retrofit.create(ApiService.class);
        Call<JSONResponse> call = request.getAll(id.trim(), ttgl.getText().toString().trim());
        call.enqueue(new Callback<JSONResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {

                JSONResponse jsonResponse = response.body();
                pesans = new ArrayList<>(Arrays.asList(jsonResponse.getPesans()));
                events = new ArrayList<>();
                popups = new ArrayList<>();
                //swipeRefreshLayout.setRefreshing(false);
                //Toast.makeText(NextEvent.this, pesans.get(0).getKet(), Toast.LENGTH_SHORT).show();
                if (pesans.size() > 0) {

                    for (int i = 0; i < pesans.size(); i++) {


                        eventColor = ContextCompat.getColor(getApplicationContext(), R.color.blue);
                        Calendar timeStart = Calendar.getInstance();
                        timeStart.set(Calendar.HOUR_OF_DAY, Integer.parseInt(pesans.get(i).getJm()));
                        timeStart.set(Calendar.MINUTE, Integer.parseInt(pesans.get(i).getMm()));
                        Calendar timeEnd = (Calendar) timeStart.clone();
                        timeEnd.set(Calendar.HOUR_OF_DAY, Integer.parseInt(pesans.get(i).getJk()));
                        timeEnd.set(Calendar.MINUTE, Integer.parseInt(pesans.get(i).getMk()));
                        Event event = new Event(i, timeStart, timeEnd, "Time\t:" + pesans.get(i).getJam_msk() + " - " + pesans.get(i).getJam_kl() + "\nName\t: " + pesans.get(i).getNama() + "\nEvent\t: " + pesans.get(i).getKet(), pesans.get(i).getNPK());
                        events.add(event);
                        dayView.setEvents(events);

                        //getNama(pesans.get(i).getNPK(),pesans.get(i).getKet(), pesans.get(i).getJam_msk(), pesans.get(i).getJam_kl(), timeStart,timeEnd );
                  /*  Popup popup = new Popup();
                    popup.setStartTime(timeStart);
                    popup.setEndTime(timeEnd);
                    popup.setImageStart("https://kiosk.akebono-astra.co.id/production/images/11289.jpg");
                    popup.setTitle(pesans.get(i).getKet());
                    popup.setDescription("Time : " + pesans.get(i).getJam_msk() + " - " + pesans.get(i).getJam_kl()+ ", PIC : " + pesans.get(i).getNPK());
                    popups.add(popup);*/
                    }
                    //dayView.setEvents(events);
                    //dayView.setPopups(popups);
                }else {
                    //Toast.makeText(NextEvent.this, "No Data", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                Log.d("Error", t.getMessage());
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG);
                tinfo.setVisibility(View.VISIBLE);
                tinfo.setText("Error Network");
            }
        });
    }

    private void getNama(String a, final String b, final String c, final String d, final Calendar e, final Calendar f) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Utils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService request = retrofit.create(ApiService.class);
        Call<JSONResponse> call = request.getUser(a.trim());
        call.enqueue(new Callback<JSONResponse>() {
            @android.support.annotation.RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {
                JSONResponse jsonResponse = response.body();
                popups = new ArrayList<>();
                users = new ArrayList<>(Arrays.asList(jsonResponse.getUsers()));
                if (users.size() > 0) {
                    Popup popup = new Popup();
                    popup.setStartTime(e);
                    popup.setEndTime(f);
                    popup.setImageStart("https://kiosk.akebono-astra.co.id/production/images/11289.jpg");
                    popup.setTitle(b);
                    popup.setDescription("Time : " + c + " - " + d+ ", Booked by : " + users.get(0).getName());
                    popups.add(popup);
                    dayView.setPopups(popups);
                }else{

                }
            }


            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                //pb.setVisibility(View.INVISIBLE);
                Log.d("Error", t.getMessage());
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_add, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }

    private boolean adaInternet(){
        ConnectivityManager koneksi = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return koneksi.getActiveNetworkInfo() != null;
    }
    private void koneksi(){
        if(adaInternet()){
            //Toast.makeText(HalamanUtama.this, "Terhubung ke internet", Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(NextEvent.this, "Tidak ada koneksi internet", Toast.LENGTH_LONG).show();
            tinfo.setVisibility(View.VISIBLE);
            tinfo.setText("Tidak ada koneksi internet!");
        }
    }
}