package com.aaij.roomstatus;

public class Waktu {
    String id;
    String jam1;
    String jam2;

    public Waktu(String jam1){
        this.jam1=jam1;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJam1() {
        return jam1;
    }

    public void setJam1(String jam1) {
        this.jam1 = jam1;
    }

    public String getJam2() {
        return jam2;
    }

    public void setJam2(String jam2) {
        this.jam2 = jam2;
    }
}
