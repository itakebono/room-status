package com.aaij.roomstatus;

public class JSONResponse {
    private Ruang[]ruangs;
    private Pesan[]pesans;
    private  User[]users;
    private User[]karyawans;

    public Ruang[]getRuangs(){return ruangs;}
    public Pesan[]getPesans(){return pesans;}
    public User[]getUsers(){return users;}

    public User[]getKaryawans(){return karyawans;}
}
