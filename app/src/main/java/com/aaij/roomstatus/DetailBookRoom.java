package com.aaij.roomstatus;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.aaij.roomstatus.calendardayview.CalendarDayView;
import com.aaij.roomstatus.calendardayview.EventView;
import com.aaij.roomstatus.calendardayview.PopupView;
import com.aaij.roomstatus.calendardayview.data.IEvent;
import com.aaij.roomstatus.calendardayview.data.IPopup;
import com.aaij.roomstatus.calendardayview.decoration.CdvDecorationDefault;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DetailBookRoom extends AppCompatActivity {

    Toolbar toolbar;
    ActionBar actionBar;
    TextView tjudul, tinfo, truang, ttgl;
    ArrayList<Pesan> pesans;
    SwipeRefreshLayout swipeRefreshLayout;
    CalendarDayView dayView;
    ArrayList<IEvent> events;
    ArrayList<IPopup> popups;
    int eventColor;
    private Calendar mCalendar;
    private int mYear, mMonth, mHour, mMinute, mDay, mSecond;
    ProgressBar pb;
    private String mDate, ntgl, jamMulai, jamSelesai;
    Handler handler;
    Runnable runnable;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Nullable
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_book_room);

        tinfo = (TextView) findViewById(R.id.textkoneksi);
        tinfo.setVisibility(View.GONE);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        truang = (TextView) findViewById(R.id.ruang);
        ttgl = (TextView) findViewById(R.id.tanggal);
        pb = (ProgressBar)findViewById(R.id.progressBar);
        pb.setVisibility(View.INVISIBLE);
        dayView = (CalendarDayView) findViewById(R.id.calendar);
        dayView.setLimitTime(7, 18);


        ((CdvDecorationDefault) (dayView.getDecoration())).setOnEventClickListener(
                new EventView.OnEventClickListener() {
                    @Override
                    public void onEventClick(EventView view, IEvent data) {
                        Log.e("TAG", "onEventClick:" + data.getName());
                        //view.getHeaderHeight();
                    }

                    @Override
                    public void onEventViewClick(View view, EventView eventView, IEvent data) {
                        Log.e("TAG", "onEventViewClick:" + data.getName());
                        if (data instanceof Event) {
                            // change event (ex: set event color)
                            dayView.setEvents(events);
                        }
                    }
                });

        ((CdvDecorationDefault) (dayView.getDecoration())).setOnPopupClickListener(
                new PopupView.OnEventPopupClickListener() {
                    @Override
                    public void onPopupClick(PopupView view, IPopup data) {
                        Log.e("TAG", "onPopupClick:" + data.getTitle());
                    }

                    @Override
                    public void onQuoteClick(PopupView view, IPopup data) {
                        Log.e("TAG", "onQuoteClick:" + data.getTitle());
                    }

                    @Override
                    public void onLoadData(PopupView view, ImageView start, ImageView end,
                                           IPopup data) {
                        start.setImageResource(R.mipmap.ic_launcher);
                    }
                });
        mCalendar = Calendar.getInstance();
        mHour = mCalendar.get(Calendar.HOUR_OF_DAY);
        mMinute = mCalendar.get(Calendar.MINUTE);
        mSecond = mCalendar.get(Calendar.SECOND);
        mYear = mCalendar.get(Calendar.YEAR);
        mMonth = mCalendar.get(Calendar.MONTH) + 1;
        mDay = mCalendar.get(Calendar.DATE);

        if(mDay < 10 && mMonth < 10){
            mDate = "0"+ mDay + "-0"+mMonth + "-" + mYear;
            ntgl = "0"+mDay + "-0"+mMonth + "-" + mYear;
        } else if(mMonth < 10){
            mDate = mDay + "-" + "0"+mMonth + "-" + mYear;
            ntgl = mDay + "-" + "0"+mMonth + "-" + mYear;
        }else if(mDay < 10){
            mDate = "0"+ mDay + "-"+mMonth + "-" + mYear;
            ntgl = "0"+mDay + "-" + "0"+mMonth + "-" + mYear;
        } else {
            mDate = mDay + "-" + mMonth + "-" + mYear;
            ntgl = mDay + "-" + mMonth + "-" + mYear;
        }
        ttgl.setText(ntgl);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        actionBar.setDisplayHomeAsUpEnabled(true);
        tjudul = (TextView) findViewById(R.id.toolbar_title);
        tjudul.setText(getIntent().getStringExtra("ruang"));
        Intent i = getIntent();
        i.getStringExtra("idruang");
        truang.setText(i.getStringExtra("idruang"));
        Toast.makeText(DetailBookRoom.this, i.getStringExtra("idruang") + ttgl.getText().toString(), Toast.LENGTH_SHORT);

        loadDatas();


        new Handler().postDelayed(new Runnable() {
            public void run() {
                loadDatas();
            }
        }, 1000);


    }

    private void loadDatas() {
        pb.setVisibility(View.VISIBLE);
        setProgressValue(1);
        koneksi();
        String id = getIntent().getStringExtra("idruang").trim();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Utils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService request = retrofit.create(ApiService.class);
        Call<JSONResponse> call = request.getDetailRuang(id, ttgl.getText().toString().trim());
        call.enqueue(new Callback<JSONResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {

                JSONResponse jsonResponse = response.body();
                pesans = new ArrayList<>(Arrays.asList(jsonResponse.getPesans()));
                events = new ArrayList<>();
                popups = new ArrayList<>();
                setProgressValue(100);
                pb.setVisibility(View.INVISIBLE);
                if (pesans.size() > 0) {
                    for (int i = 0; i < pesans.size(); i++) {
                        jamMulai = pesans.get(0).getJam_msk();
                        jamSelesai=pesans.get(0).getJam_kl();


                        eventColor = ContextCompat.getColor(getApplicationContext(), R.color.hijau);
                        Calendar timeStart = Calendar.getInstance();
                        timeStart.set(Calendar.HOUR_OF_DAY, Integer.parseInt(pesans.get(i).getJm()));
                        timeStart.set(Calendar.MINUTE, Integer.parseInt(pesans.get(i).getMm()));
                        Calendar timeEnd = (Calendar) timeStart.clone();
                        timeEnd.set(Calendar.HOUR_OF_DAY, Integer.parseInt(pesans.get(i).getJk()));
                        timeEnd.set(Calendar.MINUTE, Integer.parseInt(pesans.get(i).getMk()));
                        Event event = new Event(i,jamMulai,jamSelesai, timeStart, timeEnd);
                        events.add(event);


                    Popup popup = new Popup();
                    popup.setStartTime(timeStart);
                    popup.setEndTime(timeEnd);
                    popup.setImageStart("https://kiosk.akebono-astra.co.id/production/images/"+pesans.get(0).getNPK()+".jpg");
                    popup.setTitle(pesans.get(i).getId_ruang());
                    popup.setDescription(pesans.get(i).getKet());
                    popups.add(popup);

                    }
                    dayView.setEvents(events);
                   // dayView.setPopups(popups);
                }else {
                    Toast.makeText(DetailBookRoom.this, "No Data", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                pb.setVisibility(View.INVISIBLE);
                Log.d("Error", t.getMessage());
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG);
                tinfo.setVisibility(View.VISIBLE);
                tinfo.setText("Error Network");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_add, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }

    private boolean adaInternet(){
        ConnectivityManager koneksi = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return koneksi.getActiveNetworkInfo() != null;
    }
    private void koneksi(){
        if(adaInternet()){
            //Toast.makeText(HalamanUtama.this, "Terhubung ke internet", Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(DetailBookRoom.this, "Tidak ada koneksi internet", Toast.LENGTH_LONG).show();
            tinfo.setVisibility(View.VISIBLE);
            tinfo.setText("Tidak ada koneksi internet!");
        }
    }

    private void setProgressValue(final int progress) {
        pb.setProgress(progress);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                setProgressValue(progress + 10);
            }
        });
        thread.start();
    }

}
