package com.aaij.roomstatus;

public class jnielc {
    public static final native int ledblue(int i);

    public static final native int ledgreen(int i);

    public static final native int ledmix(int i);

    public static final native int ledoff();

    public static final native int ledoff(int i);

    public static final native int ledred(int i);

    public static final native int ledseek(int i, int i2);

    public static final native int open();

    public static final native int seekstart();

    public static final native int seekstop();

    static {
        System.loadLibrary("jnielc");
    }
}
