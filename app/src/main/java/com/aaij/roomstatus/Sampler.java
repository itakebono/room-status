package com.aaij.roomstatus;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.util.Log;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Sampler implements Runnable {
    private static volatile Sampler instance = null;
    private static final int led_blue = 3;
    private static final int led_green = 4;
    private static final int led_off = 0;
    private static final int led_on = 1;
    private static final int led_red = 2;
    private static boolean ledrunflag = false;
    private static final int seek_blue = 163;
    private static final int seek_green = 162;
    private static final int seek_red = 161;
    private ActivityManager activityManager;
    private long freq;
    private int ledCtlFlag = 2;
    private boolean mpflag = false;
    Context ncontext;
    private ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();

    private Sampler() {
    }

    public static Sampler getInstance() {
        if (instance == null) {
            synchronized (Sampler.class) {
                if (instance == null) {
                    instance = new Sampler();
                }
            }
        }
        return instance;
    }

    @SuppressLint("WrongConstant")
    public void init(Context context, long freq2) {
        this.ncontext = context;
        this.activityManager = (ActivityManager) context.getSystemService("activity");
        this.freq = freq2;
    }

    public void start() {
        ledrunflag = true;
        this.scheduler.scheduleWithFixedDelay(this, 0, this.freq, TimeUnit.MILLISECONDS);
    }

    public void stop() {
        Log.i("xiao", "xiao--ledrunflag is false");
        ledrunflag = false;
    }

    public boolean getledflag() {
        return ledrunflag;
    }

    public void run() {
        if (ledrunflag) {
            if (this.ledCtlFlag == 2) {
                this.ledCtlFlag = 3;
                jnielc.seekstart();
                jnielc.ledseek(seek_red, 50);
                jnielc.seekstop();
            } else if (this.ledCtlFlag == 3) {
                this.ledCtlFlag = 4;
                jnielc.seekstart();
                jnielc.ledseek(seek_blue, 50);
                jnielc.seekstop();
            } else if (this.ledCtlFlag == 4) {
                this.ledCtlFlag = 2;
                jnielc.seekstart();
                jnielc.ledseek(seek_green, 50);
                jnielc.seekstop();
            }
        }
        Log.i("xiao", "xiao--led Runnable-1");
    }
}
