package com.aaij.roomstatus;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.vipulasri.timelineview.TimelineView;

import java.util.List;

public class DetailAdapter extends RecyclerView.Adapter<DetailAdapter.ViewHolder>{
    private List<Pesan> pesans;
    private List<Waktu> waktus;
    Context context;

    public DetailAdapter(Context context, List<Pesan> pesans, List<Waktu> waktus) {
        this.context = context;
        this.pesans = pesans;
        this.waktus = waktus;
    }

    @Override
    public DetailAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.detail_room, parent, false);
        return new DetailAdapter.ViewHolder(view, viewType);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(DetailAdapter.ViewHolder holder, int position) {
        holder.tnpk.setText(pesans.get(position).getNPK()+" - "+pesans.get(position).getNama());
        holder.tjam.setText(waktus.get(position).getJam1());
        holder.tket.setText(pesans.get(position).getKet());

    }

    @Override
    public int getItemViewType(int position) {
        return TimelineView.getTimeLineViewType(position, getItemCount());
    }

    @Override
    public int getItemCount() {
        return pesans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tnmR, tnpk,tket, ttgl, tjam;
        LinearLayout cardView;

        public ViewHolder(final View itemView, int viewType) {
            super(itemView);

            tnpk = (TextView) itemView.findViewById(R.id.npk);
            tjam = (TextView) itemView.findViewById(R.id.wkt) ;
            tket = (TextView)itemView.findViewById(R.id.kep);
            cardView =(LinearLayout)itemView.findViewById(R.id.book);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // get position
                    int pos = getAdapterPosition();
                    if(pos != RecyclerView.NO_POSITION){
//                        }
                    }
                }
            });
        }

        @Override
        public void onClick(final View view) {

        }
    }
}
