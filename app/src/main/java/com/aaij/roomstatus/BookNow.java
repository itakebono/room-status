package com.aaij.roomstatus;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.text.format.Time;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.borax12.materialdaterangepicker.time.RadialPickerLayout;
import com.borax12.materialdaterangepicker.time.TimePickerDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BookNow extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener {
    EditText Enpk, Enama, Eno, Edate, Etimestart, Etimeend, Eevent;
    Button bSave;
    DatePickerDialog datePickerDialog;
    String ntgl,a,b,idr,appr;
    ArrayList<User> karyawans;
    ArrayList<Pesan> pesans;
    TextView Truang;
    ProgressDialog pd;
    Toolbar toolbar;
    ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.booknow_activity);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        actionBar.setDisplayHomeAsUpEnabled(true);
        //this.setFinishOnTouchOutside(true);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        idr=getIntent().getStringExtra("idruang");
        Enpk = (EditText)findViewById(R.id.npk);
        Enama = (EditText)findViewById(R.id.nama);
        Eno = (EditText)findViewById(R.id.telp);
        Edate = (EditText)findViewById(R.id.date);
        Etimestart = (EditText)findViewById(R.id.timein);
        Etimeend = (EditText)findViewById(R.id.timeout);
        Eevent = (EditText)findViewById(R.id.ket);
        bSave = (Button)findViewById(R.id.save);
        Truang = (TextView)findViewById(R.id.nmruang);
        Truang.setText(getIntent().getStringExtra("ruang"));
        //bSave.setEnabled(false);
        appr = getIntent().getStringExtra("appr");

        Enpk.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                loadData();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                loadData();
            }
        });
        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();
        Date d = new Date();
        String s  = (String) DateFormat.format("d MMMM yyyy ", d.getTime());
        String t  = (String) DateFormat.format("dd-MM-yyyy ", d.getTime());
        Edate.setText(s);
        ntgl =t;

        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        String curTime = String.format("%02d:%02d", hour, minute);
        Etimestart.setText(curTime);
        Etimeend.setText(curTime);

        Etimestart.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                final int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                android.app.TimePickerDialog mTimePicker;
                mTimePicker = new android.app.TimePickerDialog(BookNow.this, new android.app.TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        if(selectedHour < 10){
                            a="0"+selectedHour;
                        } else{
                            a= String.valueOf(selectedHour);
                        }
                        if(selectedMinute < 10){
                            b="0"+selectedMinute;
                        } else{
                            b= String.valueOf(selectedMinute);
                        }
                        Etimestart.setText(a + ":" + b);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();


            }
        });
        Etimeend.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                final int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                android.app.TimePickerDialog mTimePicker;
                mTimePicker = new android.app.TimePickerDialog(BookNow.this, new android.app.TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        if(selectedHour < 10){
                            a="0"+selectedHour;
                        } else{
                            a= String.valueOf(selectedHour);
                        }
                        if(selectedMinute < 10){
                            b="0"+selectedMinute;
                        } else{
                            b= String.valueOf(selectedMinute);
                        }
                        Etimeend.setText(a + ":" + b);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });
        Edate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateDialog();
            }
        });
        bSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(BookNow.this, "bisa", Toast.LENGTH_LONG).show();
                if(Validate()) {
                    validateTime(idr);
                }else {
                    new AlertDialog.Builder(BookNow.this)
                            .setMessage("Data belum lengkap!")
                            .setCancelable(false)
                            .setPositiveButton("BACK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                }
            }
        });

    }

    private void loadData(){
        String id = Enpk.getText().toString().trim();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Utils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiService request = retrofit.create(ApiService.class);
        Call<JSONResponse> call = request.getKaryawan(id);
        call.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {
                JSONResponse jsonResponse = response.body();
                karyawans = new ArrayList<>(Arrays.asList(jsonResponse.getKaryawans()));
                if(karyawans.size() !=0) {
                    Enama.setText(karyawans.get(0).getNama());
                    Eno.setText(karyawans.get(0).getNo_hp());
                }else {
                    Toast.makeText(BookNow.this, "Tidak ada data karyawan", Toast.LENGTH_SHORT);
                }
            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }

    private void showDate(){
        Calendar now = Calendar.getInstance();
        com.borax12.materialdaterangepicker.time.TimePickerDialog tpd = com.borax12.materialdaterangepicker.time.TimePickerDialog.newInstance(
                this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                true
        );
        tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                Log.d("TimePicker", "Dialog was cancelled");
            }
        });
        tpd.show(getFragmentManager(), "Timepickerdialog1");
    }

    private void showDateDialog(){
        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(BookNow.this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                SimpleDateFormat format = new SimpleDateFormat("d MMMM yyyy");
                String strDate = format.format(newDate.getTime());
                SimpleDateFormat format2 = new SimpleDateFormat("dd-MM-yyyy");
                String strDate2 = format2.format(newDate.getTime());
                Edate.setText(strDate);
                ntgl = strDate2;
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int hourOfDayEnd, int minuteEnd) {
        String hourString = hourOfDay < 10 ? "0"+hourOfDay : ""+hourOfDay;
        String minuteString = minute < 10 ? "0"+minute : ""+minute;
        String hourStringEnd = hourOfDayEnd < 10 ? "0"+hourOfDayEnd : ""+hourOfDayEnd;
        String minuteStringEnd = minuteEnd < 10 ? "0"+minuteEnd : ""+minuteEnd;
        String timeEnd = hourStringEnd+":"+minuteStringEnd;
        String timestart = hourString+":"+minuteString;

        Etimestart.setText(timestart);
        Etimeend.setText(timeEnd);
        //validateTime(idr);

    }

    private void insertData(){
     //   pd.show();
        String npk      = Enpk.getText().toString().trim();
        String sts;
        if(appr.equals("1")){
            sts="0";
        }else{
            sts="1";
        }
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Utils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService request = retrofit.create(ApiService.class);
        Call<Result> call = request.insertBooking(npk,Enama.getText().toString().trim(), getIntent().getStringExtra("idruang").trim(), ntgl, ntgl, Etimestart.getText().toString().trim(), Etimeend.getText().toString().trim(), Eevent.getText().toString().trim(),sts.trim());
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
        //        pd.hide();
                String value = response.body().getValue();
                String message = response.body().getMessage();
                if (value.equals("1")) {
                    //Toast.makeText(BookNow.this, message, Toast.LENGTH_SHORT).show();
                    //onBackPressed();

                    Intent intent=new Intent();
                    setResult(1,intent);
                    finish();//finishing activity

                } else {
                    //Toast.makeText(BookNow.this, message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
          //      pd.hide();
                Toast.makeText(BookNow.this, "Insert gagal, Jaringan Error!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean Validate(){
        boolean valid = true;
        String user = Enpk.getText().toString();
        String password = Eevent.getText().toString();

        if(user.isEmpty()){
            Enpk.setError("NPK is empty!");
            valid = false;
        }else {
            Enpk.setError(null);
        }
        if(password.isEmpty()){
            Eevent.setError("Password is empty!");
            valid = false;
        }
        return valid;
    }


    private Boolean validateTime(final String idruang){
        Boolean validateTime = true;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Utils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiService request = retrofit.create(ApiService.class);
        Call<JSONResponse> call = request.getTimeValid(idruang.trim(), ntgl.trim(), Etimestart.getText().toString().trim(), Etimeend.getText().toString().trim());
        call.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {
                JSONResponse jsonResponse = response.body();
                pesans = new ArrayList<>(Arrays.asList(jsonResponse.getPesans()));
                if(pesans.size() !=0) {
                    new AlertDialog.Builder(BookNow.this)
                            .setMessage("Room was Booked on that time\nPlease select other Room/Date/Time")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                }else {
                    //bSave.setEnabled(true);
                    Toast.makeText(BookNow.this, "Room Ready To Book", Toast.LENGTH_SHORT).show();
                    insertData();
                }
            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
        return validateTime;
    }
    @Override
    public void onResume(){
        super.onResume();


    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:
                this.getWindow().getDecorView().setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                onBackPressed();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        this.getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        super.onBackPressed();
    }
}
