package com.aaij.roomstatus;

public class Pesan {
    String id_book;
    String NPK;
    String id_ruang;
    String tgl_msk;
    String tgl_kl;
    String jam_msk;
    String jam_kl;
    String jm;
    String jk;
    String mm;
    String mk;
    String ket;
    String nama;
    String book_status;
    String done_status;
    String status_waiting;
    String nama_ruang;
    String status;
    String dept;
    String waktu;
    String jumlah;

    public Pesan(String NPK, String dept, String ket) {
        this.NPK = NPK;
        this.dept =dept;
        this.ket=ket;
    }
    public Pesan(String jam_msk, String jam_kl, String ket, String nama) {
        this.jam_msk = jam_msk;
        this.jam_kl =jam_kl;
        this.ket=ket;
        this.nama=nama;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }

    public String getMm() {
        return mm;
    }

    public void setMm(String mm) {
        this.mm = mm;
    }

    public String getMk() {
        return mk;
    }

    public void setMk(String mk) {
        this.mk = mk;
    }

    public Pesan(String waktu) {
        this.waktu = waktu;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getStatus_waiting() {
        return status_waiting;
    }

    public void setStatus_waiting(String status_waiting) {
        this.status_waiting = status_waiting;
    }

    public String getJm() {
        return jm;
    }

    public void setJm(String jm) {
        this.jm = jm;
    }

    public String getJk() {
        return jk;
    }

    public void setJk(String jk) {
        this.jk = jk;
    }

    public String getId_book() {
        return id_book;
    }

    public void setId_book(String id_book) {
        this.id_book = id_book;
    }

    public String getNPK() {
        return NPK;
    }

    public String getJam_msk() {
        return jam_msk;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setJam_msk(String jam_msk) {
        this.jam_msk = jam_msk;
    }

    public String getJam_kl() {
        return jam_kl;
    }

    public void setJam_kl(String jam_kl) {
        this.jam_kl = jam_kl;
    }

    public String getKet() {
        return ket;
    }

    public void setKet(String ket) {
        this.ket = ket;
    }

    public void setNPK(String npk) {
        this.NPK = NPK;
    }

    public String getId_ruang() {
        return id_ruang;
    }

    public void setId_ruang(String id_ruang) {
        this.id_ruang = id_ruang;
    }

    public String getTgl_msk() {
        return tgl_msk;
    }

    public void setTgl_msk(String tgl_msk) {
        this.tgl_msk = tgl_msk;
    }

    public String getTgl_kl() {
        return tgl_kl;
    }

    public void setTgl_kl(String tgl_kl) {
        this.tgl_kl = tgl_kl;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBook_status() {
        return book_status;
    }

    public void setBook_status(String book_status) {
        this.book_status = book_status;
    }

    public String getDone_status() {
        return done_status;
    }

    public void setDone_status(String done_status) {
        this.done_status = done_status;
    }

    public String getNama_ruang() {
        return nama_ruang;
    }

    public void setNama_ruang(String nama_ruang) {
        this.nama_ruang = nama_ruang;
    }
}
