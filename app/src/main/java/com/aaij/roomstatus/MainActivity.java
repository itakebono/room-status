package com.aaij.roomstatus;

import android.app.ActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.ConnectivityManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ArrayList<Ruang> ruangs;
    RuangAdapter ruangAdapter;
    TextView tinfo;
    ProgressBar pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        recyclerView = (RecyclerView)findViewById(R.id.recyclerview);
        tinfo = (TextView)findViewById(R.id.textkoneksi);
        pb = (ProgressBar)findViewById(R.id.progressBar);
        pb.setVisibility(View.INVISIBLE);
        tinfo.setVisibility(View.INVISIBLE);
        recyclerView.setHasFixedSize(true);
        StaggeredGridLayoutManager gridLayoutManager =
                new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        gridLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(gridLayoutManager);
        loadData();
    }
    private void loadData(){
        koneksi();
        pb.setVisibility(View.VISIBLE);
        pb.getIndeterminateDrawable()
                .setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_IN);
        setProgressValue(1);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Utils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final ApiService request = retrofit.create(ApiService.class);
        Call<JSONResponse> call = request.getRuangan();
        call.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {

                JSONResponse jsonResponse = response.body();
                ruangs = new ArrayList<>(Arrays.asList(jsonResponse.getRuangs()));
                ruangAdapter = new RuangAdapter(getApplicationContext(), ruangs);
                if (ruangAdapter.getItemCount() != 0) {
                    setProgressValue(100);
                    pb.setVisibility(View.GONE);
                    recyclerView.setAdapter(ruangAdapter);

                } else {
                    pb.setVisibility(View.GONE);
                    tinfo.setVisibility(View.VISIBLE);
                    tinfo.setText("No Data");
                }
            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                tinfo.setVisibility(View.INVISIBLE);
                pb.setVisibility(View.GONE);
                Log.d("Error", t.getMessage());
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG);
                tinfo.setVisibility(View.VISIBLE);
                tinfo.setText("Error Network");
            }
        });

    }

    private void setProgressValue(final int progress) {
        pb.setProgress(progress);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                setProgressValue(progress + 10);
            }
        });
        thread.start();
    }

    private boolean adaInternet(){
        ConnectivityManager koneksi = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return koneksi.getActiveNetworkInfo() != null;
    }
    private void koneksi(){
        if(adaInternet()){
            //Toast.makeText(HalamanUtama.this, "Terhubung ke internet", Toast.LENGTH_LONG).show();
        }else{
            //Toast.makeText(getApplicationContext(), "Tidak ada koneksi internet", Toast.LENGTH_LONG).show();
            tinfo.setVisibility(View.VISIBLE);
            tinfo.setText("Tidak ada koneksi internet!");
        }
    }

    @Override
    public void onBackPressed() {
        final AlertDialog.Builder dNote = new AlertDialog.Builder(MainActivity.this);
        dNote.setTitle("Confirmation\n\n");
        LinearLayout layout = new LinearLayout(MainActivity.this);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setPadding(25,25,25,25);
        layout.setBackgroundColor(Color.parseColor("#eeeeee"));
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        lp.setMargins(0,0,0,30);
        final TextView theader1 = new TextView(MainActivity.this);
        theader1.setText("Password");
        theader1.setTextColor(Color.BLACK);
        theader1.setTextSize(20);
        final EditText Epass = new EditText(MainActivity.this);
        Epass.setHint("Password");
        Epass.setTransformationMethod(PasswordTransformationMethod.getInstance());
        Epass.setTextColor(Color.BLACK);
        Epass.setTextSize(20);

        layout.addView(theader1);
        layout.addView(Epass);

        dNote.setView(layout);
        dNote.setNeutralButton("CANCEL",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int which) {
                        dialog.dismiss();
                    }});
        dNote.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int which) {
                        if(Epass.getText().toString().equals("kosongin")) {
                            dialog.dismiss();
                            startActivity(new Intent(MainActivity.this, RoomDetail.class));
                        }else{
                            dialog.dismiss();
                        }
                    }});
        dNote.show();
    }
    @Override
    protected void onPause() {
        super.onPause();

        ActivityManager activityManager = (ActivityManager) getApplicationContext()
                .getSystemService(Context.ACTIVITY_SERVICE);

        //activityManager.moveTaskToFront(getTaskId(), 0);
    }

}
