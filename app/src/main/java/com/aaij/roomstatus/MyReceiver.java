package com.aaij.roomstatus;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class MyReceiver extends BroadcastReceiver {
    private static final String ACTION = "android.intent.action.MEDIA_MOUNTED";
    private static final String ACTION_BOOT = "android.intent.action.BOOT_COMPLETED";

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(ACTION_BOOT)) {
            Sampler.getInstance().init(context, 5000);
            Sampler.getInstance().start();
        }
    }
}
