package com.aaij.roomstatus;

import java.lang.reflect.InvocationTargetException;

public class SystemProperteisProxy {
    public static String getString(String key) {
        return (String) getProxy(key, String.class);
    }

    public static String getString(String key, String def) {
        return (String) getDefaultProxy(key, "get", String.class, def);
    }

    public static int getInt(String key, int def) {
        return ((Integer) getDefaultProxy(key, "getInt", Integer.class, Integer.valueOf(def))).intValue();
    }

    public static long getLong(String key, long def) {
        return ((Long) getDefaultProxy(key, "getLong", Long.class, Long.valueOf(def))).longValue();
    }

    public static boolean getBoolean(String key, boolean def) {
        return ((Boolean) getDefaultProxy(key, "getBoolean", Boolean.class, Boolean.valueOf(def))).booleanValue();
    }

    public static void set(String key, String val) {
        setProxy(key, val);
    }

    private static void setProxy(String key, String val) {
        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            c.getMethod("set", new Class[]{String.class, String.class}).invoke(c, new Object[]{key, val});
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e2) {
            e2.printStackTrace();
        } catch (IllegalAccessException e3) {
            e3.printStackTrace();
        } catch (IllegalArgumentException e4) {
            e4.printStackTrace();
        } catch (InvocationTargetException e5) {
            e5.printStackTrace();
        }
    }

    private static <T> Object getProxy(String key, Class<T> paramsClass) {
        T result = null;
        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            return  c.getMethod("get", new Class[]{paramsClass}).invoke(c, new Object[]{key});
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return result;
        } catch (NoSuchMethodException e2) {
            e2.printStackTrace();
            return result;
        } catch (IllegalAccessException e3) {
            e3.printStackTrace();
            return result;
        } catch (IllegalArgumentException e4) {
            e4.printStackTrace();
            return result;
        } catch (InvocationTargetException e5) {
            e5.printStackTrace();
            return result;
        }
    }

    private static <T> Object getDefaultProxy(String key, String methodName, Class<T> paramsClass, T defaultValue) {
        T result = defaultValue;
        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            return c.getMethod(methodName, new Class[]{String.class, paramsClass}).invoke(c, new Object[]{key, defaultValue});
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return result;
        } catch (NoSuchMethodException e2) {
            e2.printStackTrace();
            return result;
        } catch (IllegalAccessException e3) {
            e3.printStackTrace();
            return result;
        } catch (IllegalArgumentException e4) {
            e4.printStackTrace();
            return result;
        } catch (InvocationTargetException e5) {
            e5.printStackTrace();
            return result;
        }
    }
}

